// Fill out your copyright notice in the Description page of Project Settings.


#include "System/MetaAssetManager.h"

UMetaAssetManager::UMetaAssetManager()
{
	DefaultPawnData = nullptr;
}

UMetaAssetManager& UMetaAssetManager::Get()
{
	check(GEngine);

	if (UMetaAssetManager* Singleton = Cast<UMetaAssetManager>(GEngine->AssetManager))
	{
		UE_LOG(LogTemp, Warning, TEXT("UMetaAssetManager::Get()"));
		return *Singleton;
	}

	// Fatal error above prevents this from being called.
	return *NewObject<UMetaAssetManager>();
}
