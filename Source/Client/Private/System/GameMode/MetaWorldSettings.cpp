// Fill out your copyright notice in the Description page of Project Settings.


#include "System/GameMode/MetaWorldSettings.h"
#include "Engine/AssetManager.h"

FPrimaryAssetId AMetaWorldSettings::GetDefaultGameplayExperience() const
{
	FPrimaryAssetId Result;
	if (!DefaultGameplayExperience.IsNull())
	{
		Result = UAssetManager::Get().GetPrimaryAssetIdForPath(DefaultGameplayExperience.ToSoftObjectPath());
	}
	return Result;
}
