// Fill out your copyright notice in the Description page of Project Settings.


#include "System/GameMode/MetaGameState.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "System/GameMode/MetaExperienceManagerComponent.h"

AMetaGameState::AMetaGameState(const FObjectInitializer& ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//AbilitySystemComponent = ObjectInitializer.CreateDefaultSubobject<UMetaAbilitySystemComponent>(this, TEXT("AbilitySystemComponent"));
	//AbilitySystemComponent->SetIsReplicated(true);
	//AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	ExperienceManagerComponent = CreateDefaultSubobject<UMetaExperienceManagerComponent>(TEXT("ExperienceManagerComponent"));

	//ServerFPS = 0.0f;
}

void AMetaGameState::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void AMetaGameState::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(LogTemp, Warning, TEXT("AMetaGameState::PostInitializeComponents"));
}

void AMetaGameState::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AMetaGameState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

UAbilitySystemComponent* AMetaGameState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}
