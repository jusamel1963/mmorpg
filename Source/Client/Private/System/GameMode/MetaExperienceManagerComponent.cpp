// Fill out your copyright notice in the Description page of Project Settings.


#include "System/GameMode/MetaExperienceManagerComponent.h"
#include "Net/UnrealNetwork.h"
#include "System/MetaAssetManager.h"
#include "System/GameMode/MetaExperienceDefinition.h"

bool UMetaExperienceManagerComponent::IsExperienceLoaded() const
{
	return (LoadState == EMetaExperienceLoadState::Loaded) && (CurrentExperience != nullptr);
}

void UMetaExperienceManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(ThisClass, CurrentExperience);
}

const UMetaExperienceDefinition* UMetaExperienceManagerComponent::GetCurrentExperienceChecked() const
{
	check(LoadState == EMetaExperienceLoadState::Loaded);
	check(CurrentExperience != nullptr);
	return CurrentExperience;
}

void UMetaExperienceManagerComponent::SetCurrentExperience(FPrimaryAssetId ExperienceId)
{
	UMetaAssetManager& AssetManager = UMetaAssetManager::Get();
	FSoftObjectPath AssetPath = AssetManager.GetPrimaryAssetPath(ExperienceId);
	TSubclassOf<UMetaExperienceDefinition> AssetClass = Cast<UClass>(AssetPath.TryLoad());
	check(AssetClass);
	const UMetaExperienceDefinition* Experience = GetDefault<UMetaExperienceDefinition>(AssetClass);

	check(Experience != nullptr);
	check(CurrentExperience == nullptr);
	CurrentExperience = Experience;
	StartExperienceLoad();
}

void UMetaExperienceManagerComponent::OnRep_CurrentExperience()
{
	StartExperienceLoad();
}

void UMetaExperienceManagerComponent::StartExperienceLoad()
{
	check(CurrentExperience != nullptr);
	check(LoadState == EMetaExperienceLoadState::Unloaded);


	LoadState = EMetaExperienceLoadState::Loading;

	OnExperienceFullLoadCompleted();
}

void UMetaExperienceManagerComponent::OnExperienceFullLoadCompleted()
{
	check(LoadState != EMetaExperienceLoadState::Loaded);

	LoadState = EMetaExperienceLoadState::ExecutingActions;

	LoadState = EMetaExperienceLoadState::Loaded;

	OnExperienceLoaded_HighPriority.Broadcast(CurrentExperience);
	OnExperienceLoaded_HighPriority.Clear();

	OnExperienceLoaded.Broadcast(CurrentExperience);
	OnExperienceLoaded.Clear();

	OnExperienceLoaded_LowPriority.Broadcast(CurrentExperience);
	OnExperienceLoaded_LowPriority.Clear();

}

void UMetaExperienceManagerComponent::CallOrRegister_OnExperienceLoaded(FOnMetaExperienceLoaded::FDelegate&& Delegate)
{
	if (IsExperienceLoaded())
	{
		Delegate.Execute(CurrentExperience);
	}
	else
	{
		OnExperienceLoaded.Add(MoveTemp(Delegate));
	}
}
