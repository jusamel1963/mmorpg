// Fill out your copyright notice in the Description page of Project Settings.


#include "System/GameInstance/MetaGameInstance.h"
#include "Components/GameFrameworkComponentManager.h"
#include "System/MetaGameplayTags.h"

void UMetaGameInstance::Init()
{
	Super::Init();

	// Register our custom init states
	UGameFrameworkComponentManager* ComponentManager = GetSubsystem<UGameFrameworkComponentManager>(this);

	if (ensure(ComponentManager))
	{
		ComponentManager->RegisterInitState(MetaGameplayTags::InitState_Spawned, false, FGameplayTag());
		ComponentManager->RegisterInitState(MetaGameplayTags::InitState_DataAvailable, false, MetaGameplayTags::InitState_Spawned);
		ComponentManager->RegisterInitState(MetaGameplayTags::InitState_DataInitialized, false, MetaGameplayTags::InitState_DataAvailable);
		ComponentManager->RegisterInitState(MetaGameplayTags::InitState_GameplayReady, false, MetaGameplayTags::InitState_DataInitialized);
	}
}
