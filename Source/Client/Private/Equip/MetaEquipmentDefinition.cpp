// Fill out your copyright notice in the Description page of Project Settings.


#include "Equip/MetaEquipmentDefinition.h"
#include "Equip/MetaEquipmentInstance.h"

UMetaEquipmentDefinition::UMetaEquipmentDefinition(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InstanceType = UMetaEquipmentInstance::StaticClass();
}
