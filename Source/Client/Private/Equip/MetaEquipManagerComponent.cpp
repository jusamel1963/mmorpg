// Fill out your copyright notice in the Description page of Project Settings.


#include "Equip/MetaEquipManagerComponent.h"
#include "Equip/MetaEquipmentInstance.h"
#include "Net/UnrealNetwork.h"
#include "Equip/MetaEquipmentDefinition.h"
#include "Components/ActorComponent.h"
#include "AbilitySystemGlobals.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "Engine/ActorChannel.h"

UMetaEquipManagerComponent::UMetaEquipManagerComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, EquipmentList(this)
{
	bReplicateUsingRegisteredSubObjectList = true;
	SetIsReplicatedByDefault(true);
}

UMetaEquipmentInstance* UMetaEquipManagerComponent::EquipItem(TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition)
{
	UMetaEquipmentInstance* Result = nullptr;
	if (EquipmentDefinition != nullptr)
	{
		Result = EquipmentList.AddEntry(EquipmentDefinition);
		if (Result != nullptr)
		{
			Result->OnEquipped();

			if (IsUsingRegisteredSubObjectList() && IsReadyForReplication())
			{
				AddReplicatedSubObject(Result);
			}
		}
	}
	return Result;
}

void UMetaEquipManagerComponent::UnequipItem(UMetaEquipmentInstance* ItemInstance)
{
	if (ItemInstance != nullptr)
	{
		if (IsUsingRegisteredSubObjectList())
		{
			RemoveReplicatedSubObject(ItemInstance);
		}

	    ItemInstance->OnUnequipped();
		EquipmentList.RemoveEntry(ItemInstance);
	}
}

void UMetaEquipManagerComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

void UMetaEquipManagerComponent::UninitializeComponent()
{
	Super::UninitializeComponent();
}

void UMetaEquipManagerComponent::ReadyForReplication()
{
	Super::ReadyForReplication();

	// Register existing MetaEquipmentInstances
	if (IsUsingRegisteredSubObjectList())
	{
		for (const FMetaAppliedEquipmentEntry& Entry : EquipmentList.Entries)
		{
			UMetaEquipmentInstance* Instance = Entry.Instance;

			if (IsValid(Instance))
			{
				AddReplicatedSubObject(Instance);
			}
		}
	}
}

void UMetaEquipManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, EquipmentList);
}

bool UMetaEquipManagerComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (FMetaAppliedEquipmentEntry& Entry : EquipmentList.Entries)
	{
		UMetaEquipmentInstance* Instance = Entry.Instance;

		if (IsValid(Instance))
		{
			WroteSomething |= Channel->ReplicateSubobject(Instance, *Bunch, *RepFlags);
		}
	}

	return WroteSomething;
}

UMetaEquipmentInstance* UMetaEquipManagerComponent::GetFirstInstanceOfType(TSubclassOf<UMetaEquipmentInstance> InstanceType)
{
	for (FMetaAppliedEquipmentEntry& Entry : EquipmentList.Entries)
	{
		if (UMetaEquipmentInstance* Instance = Entry.Instance)
		{
			if (Instance->IsA(InstanceType))
			{
				return Instance;
			}
		}
	}

	return nullptr;
}

UMetaEquipmentInstance* FMetaEquipmentList::AddEntry(TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition)
{
	UMetaEquipmentInstance* Result = nullptr;

	check(EquipmentDefinition != nullptr);
	check(OwnerComponent);
	check(OwnerComponent->GetOwner()->HasAuthority());

	const UMetaEquipmentDefinition* EquipmentCDO = GetDefault<UMetaEquipmentDefinition>(EquipmentDefinition);

	TSubclassOf<UMetaEquipmentInstance> InstanceType = EquipmentCDO->InstanceType;
	if (InstanceType == nullptr)
	{
		InstanceType = UMetaEquipmentInstance::StaticClass();
	}

	FMetaAppliedEquipmentEntry& NewEntry = Entries.AddDefaulted_GetRef();
	NewEntry.EquipmentDefinition = EquipmentDefinition;
	NewEntry.Instance = NewObject<UMetaEquipmentInstance>(OwnerComponent->GetOwner(), InstanceType);  //@TODO: Using the actor instead of component as the outer due to UE-127172
	Result = NewEntry.Instance;

	if (UMetaAbilitySystemComponent* ASC = GetAbilitySystemComponent())
	{
		for (const TObjectPtr<const UMetaAbilitySet>& AbilitySet : EquipmentCDO->AbilitySetsToGrant)
		{
			AbilitySet->GiveToAbilitySystem(ASC, /*inout*/ &NewEntry.GrantedHandles, Result);
		}
	}
	else
	{
		//@TODO: Warning logging?
	}

	Result->SpawnEquipmentActors(EquipmentCDO->ActorsToSpawn);


	MarkItemDirty(NewEntry);

	return Result;
}

void FMetaEquipmentList::RemoveEntry(UMetaEquipmentInstance* Instance)
{
}

UMetaAbilitySystemComponent* FMetaEquipmentList::GetAbilitySystemComponent() const
{
	check(OwnerComponent);
	AActor* OwningActor = OwnerComponent->GetOwner();
	return Cast<UMetaAbilitySystemComponent>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OwningActor));
}
