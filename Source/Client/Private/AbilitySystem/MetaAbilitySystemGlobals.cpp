// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/MetaAbilitySystemGlobals.h"
#include "AbilitySystem/MetaGameplayEffectContext.h"

FGameplayEffectContext* UMetaAbilitySystemGlobals::AllocGameplayEffectContext() const
{
	return new FMetaGameplayEffectContext();
}