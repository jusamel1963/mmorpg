// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/MetaGameplayEffectContext.h"
#include "AbilitySystem/MetaAbilitySourceInterface.h"
#include "Engine/HitResult.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

#if UE_WITH_IRIS
#include "Iris/ReplicationState/PropertyNetSerializerInfoRegistry.h"
#include "Serialization/GameplayEffectContextNetSerializer.h"
#endif

FMetaGameplayEffectContext* FMetaGameplayEffectContext::ExtractEffectContext(struct FGameplayEffectContextHandle Handle)
{
	FGameplayEffectContext* BaseEffectContext = Handle.Get();
	if ((BaseEffectContext != nullptr) && BaseEffectContext->GetScriptStruct()->IsChildOf(FMetaGameplayEffectContext::StaticStruct()))
	{
		return (FMetaGameplayEffectContext*)BaseEffectContext;
	}

	return nullptr;
}

bool FMetaGameplayEffectContext::NetSerialize(FArchive& Ar, UPackageMap* Map, bool& bOutSuccess)
{
	FGameplayEffectContext::NetSerialize(Ar, Map, bOutSuccess);
	return true;
}

#if UE_WITH_IRIS
namespace UE::Net
{
	// Forward to FGameplayEffectContextNetSerializer
	// Note: If FLyraGameplayEffectContext::NetSerialize() is modified, a custom NetSerializesr must be implemented as the current fallback will no longer be sufficient.
	UE_NET_IMPLEMENT_FORWARDING_NETSERIALIZER_AND_REGISTRY_DELEGATES(MetaGameplayEffectContext, FGameplayEffectContextNetSerializer);
}
#endif

void FMetaGameplayEffectContext::SetAbilitySource(const IMetaAbilitySourceInterface* InObject, float InSourceLevel)
{
	AbilitySourceObject = MakeWeakObjectPtr(Cast<const UObject>(InObject));
	//SourceLevel = InSourceLevel;
}

const IMetaAbilitySourceInterface* FMetaGameplayEffectContext::GetAbilitySource() const
{
	return Cast<IMetaAbilitySourceInterface>(AbilitySourceObject.Get());
}

const UPhysicalMaterial* FMetaGameplayEffectContext::GetPhysicalMaterial() const
{
	if (const FHitResult* HitResultPtr = GetHitResult())
	{
		return HitResultPtr->PhysMaterial.Get();
	}
	return nullptr;
}
