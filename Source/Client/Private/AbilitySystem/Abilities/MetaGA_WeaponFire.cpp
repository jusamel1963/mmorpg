// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Abilities/MetaGA_WeaponFire.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"

void UMetaGA_WeaponFire::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	UE_LOG(LogTemp, Warning, TEXT("UMetaGA_WeaponFire::ActivateAbility"));
	if (IsLocallyControlled())
	{
		StartRangedWeaponTargeting();
	}
	UAbilityTask_PlayMontageAndWait* PlayMontageAndWaitTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, NAME_None, AM_CharacterFire, 1.0f, NAME_None, false, 1.0f);
	PlayMontageAndWaitTask->OnCompleted.AddDynamic(this, &UMetaGA_WeaponFire::OnAMCompleted);
	PlayMontageAndWaitTask->OnInterrupted.AddDynamic(this, &UMetaGA_WeaponFire::OnAMInterrupted);
	PlayMontageAndWaitTask->OnCancelled.AddDynamic(this, &UMetaGA_WeaponFire::OnAMCancelled);
	PlayMontageAndWaitTask->ReadyForActivation();
}

void UMetaGA_WeaponFire::OnRangedWeaponTargetDataReady(const FGameplayAbilityTargetDataHandle& TargetData)
{
	if (HasAuthority(&CurrentActivationInfo))
	{
		ApplyGameplayEffectToTarget(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, TargetData, GE_Damage, 1, 1);
	}
}

void UMetaGA_WeaponFire::OnAMCompleted()
{
	K2_EndAbility();
}

void UMetaGA_WeaponFire::OnAMInterrupted()
{
	K2_EndAbility();
}

void UMetaGA_WeaponFire::OnAMCancelled()
{
	K2_EndAbility();
}
