// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Abilities/MetaGA_Jump.h"
#include "Characters/MetaCharacter.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "Abilities/Tasks/AbilityTask_StartAbilityState.h"

UMetaGA_Jump::UMetaGA_Jump(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UMetaGA_Jump::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	CharacterJumpStart();
	UAbilityTask_StartAbilityState* JumpingState = UAbilityTask_StartAbilityState::StartAbilityState(this, FName("Jumping"), true);
	JumpingState->OnStateEnded.AddDynamic(this, &UMetaGA_Jump::OnJumpingEnded);
	JumpingState->OnStateInterrupted.AddDynamic(this, &UMetaGA_Jump::OnJumpingInterrupted);
	JumpingState->ReadyForActivation();

	UAbilityTask_WaitInputRelease* InputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this,true);
	InputReleaseTask->OnRelease.AddDynamic(this, &UMetaGA_Jump::HandleInputRelease);
	InputReleaseTask->ReadyForActivation();
}

bool UMetaGA_Jump::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!ActorInfo || !ActorInfo->AvatarActor.IsValid())
	{
		return false;
	}

	const AMetaCharacter* MetaCharacter = Cast<AMetaCharacter>(ActorInfo->AvatarActor.Get());
	if (!MetaCharacter || !MetaCharacter->CanJump())
	{
		return false;
	}

	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	return true;
}

void UMetaGA_Jump::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{	
	// Stop jumping in case the ability blueprint doesn't call it.
	CharacterJumpStop();

	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UMetaGA_Jump::CharacterJumpStart()
{
	if (AMetaCharacter* MetaCharacter = GetMetaCharacterFromActorInfo())
	{
		if (MetaCharacter->IsLocallyControlled() && !MetaCharacter->bPressedJump)
		{
			MetaCharacter->UnCrouch();
			MetaCharacter->Jump();
		}
	}
}

void UMetaGA_Jump::CharacterJumpStop()
{
	if (AMetaCharacter* MetaCharacter = GetMetaCharacterFromActorInfo())
	{
		if (MetaCharacter->IsLocallyControlled() && MetaCharacter->bPressedJump)
		{
			MetaCharacter->StopJumping();
		}
	}
}

void UMetaGA_Jump::OnJumpingEnded()
{
	CharacterJumpStop();
}

void UMetaGA_Jump::OnJumpingInterrupted()
{
	CharacterJumpStop();
}

void UMetaGA_Jump::HandleInputRelease(float TimeHeld)
{
	K2_EndAbility();
}
