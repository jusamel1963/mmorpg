// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Abilities/MetaGA_FromEquipment.h"
#include "Equip/MetaEquipmentInstance.h"

UMetaGA_FromEquipment::UMetaGA_FromEquipment(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

UMetaEquipmentInstance* UMetaGA_FromEquipment::GetAssociatedEquipment() const
{
	if (FGameplayAbilitySpec* Spec = UGameplayAbility::GetCurrentAbilitySpec())
	{
		return Cast<UMetaEquipmentInstance>(Spec->SourceObject.Get());
	}

	return nullptr;
}