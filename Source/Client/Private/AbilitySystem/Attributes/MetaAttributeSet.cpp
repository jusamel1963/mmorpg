// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Attributes/MetaAttributeSet.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"

UMetaAttributeSet::UMetaAttributeSet()
{
}

UWorld* UMetaAttributeSet::GetWorld() const
{
	const UObject* Outer = GetOuter();
	check(Outer);

	return Outer->GetWorld();
}

UMetaAbilitySystemComponent* UMetaAttributeSet::GetMetaAbilitySystemComponent() const
{
	return Cast<UMetaAbilitySystemComponent>(GetOwningAbilitySystemComponent());
}

