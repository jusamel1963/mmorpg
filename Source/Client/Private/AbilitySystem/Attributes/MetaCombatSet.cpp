// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Attributes/MetaCombatSet.h"
#include "Net/UnrealNetwork.h"

UMetaCombatSet::UMetaCombatSet()
	: BaseDamage(0.0f)
	, BaseHeal(0.0f)
{
}

void UMetaCombatSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UMetaCombatSet, BaseDamage, COND_OwnerOnly, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UMetaCombatSet, BaseHeal, COND_OwnerOnly, REPNOTIFY_Always);
}

void UMetaCombatSet::OnRep_BaseDamage(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UMetaCombatSet, BaseDamage, OldValue);
}

void UMetaCombatSet::OnRep_BaseHeal(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UMetaCombatSet, BaseHeal, OldValue);
}
