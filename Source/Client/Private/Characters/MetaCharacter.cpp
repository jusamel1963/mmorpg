// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/MetaCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Characters/MetaPawnExtensionComponent.h"
#include "Characters/MetaHeroComponent.h"
#include "Characters/MetaHealthComponent.h"
#include "TimerManager.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "Equip/MetaEquipManagerComponent.h"


#include "Inventory/MetaInventoryManagerComponent.h"
#include "Equip/MetaQuickBarComponent.h"
#include "Equip/MetaPickupDefinition.h"
#include "Inventory/MetaInventoryItemInstance.h"

AMetaCharacter::AMetaCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PawnExtComponent = CreateDefaultSubobject<UMetaPawnExtensionComponent>(TEXT("PawnExtensionComponent"));
	if (IsValid(PawnExtComponent))
	{
		PawnExtComponent->OnAbilitySystemInitialized_RegisterAndCall(FSimpleMulticastDelegate::FDelegate::CreateUObject(this, &ThisClass::OnAbilitySystemInitialized));
		PawnExtComponent->OnAbilitySystemUninitialized_Register(FSimpleMulticastDelegate::FDelegate::CreateUObject(this, &ThisClass::OnAbilitySystemUninitialized));
	}

	MetaHeroComponent = CreateDefaultSubobject<UMetaHeroComponent>(TEXT("MetaHeroComponent"));

	HealthComponent = CreateDefaultSubobject<UMetaHealthComponent>(TEXT("HealthComponent"));
	if (IsValid(HealthComponent))
	{
		HealthComponent->OnDeathStarted.AddDynamic(this, &ThisClass::OnDeathStarted);
		HealthComponent->OnDeathFinished.AddDynamic(this, &ThisClass::OnDeathFinished);
	}

	EquipManagerComponent = CreateDefaultSubobject<UMetaEquipManagerComponent>(TEXT("EquipManagerComponent"));

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

UAbilitySystemComponent* AMetaCharacter::GetAbilitySystemComponent() const
{
	if (PawnExtComponent == nullptr)
	{
		return nullptr;
	}

	return PawnExtComponent->GetMetaAbilitySystemComponent();
}

void AMetaCharacter::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
}

bool AMetaCharacter::HasMatchingGameplayTag(FGameplayTag TagToCheck) const
{
	return false;
}

bool AMetaCharacter::HasAllMatchingGameplayTags(const FGameplayTagContainer& TagContainer) const
{
	return false;
}

bool AMetaCharacter::HasAnyMatchingGameplayTags(const FGameplayTagContainer& TagContainer) const
{
	return false;
}

// ClientCharacter
void AMetaCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
}

//////////////////////////////////////////////////////////////////////////
// Input

UMetaAbilitySystemComponent* AMetaCharacter::GetMetaAbilitySystemComponent() const
{
	return Cast<UMetaAbilitySystemComponent>(GetAbilitySystemComponent());
}

void AMetaCharacter::FellOutOfWorld(const UDamageType& dmgType)
{
	HealthComponent->DamageSelfDestruct(/*bFellOutOfWorld=*/ true);
}

void AMetaCharacter::GiveInitWeapon_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("GiveInitWeapon"));
	if (!IsValid(InitPickupDefinition))
	{
		return;
	}

	if (AController* OwnController = GetController())
	{
		UMetaInventoryManagerComponent* InventoryManagerComponent = OwnController->FindComponentByClass<UMetaInventoryManagerComponent>();
		UMetaQuickBarComponent* QuickBarComponent = OwnController->FindComponentByClass<UMetaQuickBarComponent>();
		if (InventoryManagerComponent && QuickBarComponent)
		{
			UMetaInventoryItemInstance* InventoryItemInstance = InventoryManagerComponent->AddItemDefinition(InitPickupDefinition->InventoryItemDefinition);
			QuickBarComponent->AddItemToSlot(0, InventoryItemInstance);
			QuickBarComponent->SetActiveSlotIndex(0);
		}
	}
	
}

void AMetaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PawnExtComponent->SetupPlayerInputComponent();
	MetaHeroComponent->InitializePlayerInput(PlayerInputComponent);
}

void AMetaCharacter::OnAbilitySystemInitialized()
{
	UMetaAbilitySystemComponent* MetaASC = GetMetaAbilitySystemComponent();
	check(MetaASC);

	HealthComponent->InitializeWithAbilitySystem(MetaASC);
}

void AMetaCharacter::OnAbilitySystemUninitialized()
{
	HealthComponent->UninitializeFromAbilitySystem();
}

void AMetaCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	PawnExtComponent->HandleControllerChanged();
}

void AMetaCharacter::UnPossessed()
{
	Super::UnPossessed();
	PawnExtComponent->HandleControllerChanged();
}

void AMetaCharacter::OnRep_Controller()
{
	Super::OnRep_Controller();

	PawnExtComponent->HandleControllerChanged();
}

void AMetaCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	PawnExtComponent->HandlePlayerStateReplicated();
}

void AMetaCharacter::OnDeathStarted(AActor* OwningActor)
{
	//DisableMovementAndCollision();
}

void AMetaCharacter::OnDeathFinished(AActor* OwningActor)
{
	//GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ThisClass::DestroyDueToDeath);
}
