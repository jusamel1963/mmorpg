// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/MetaHeroComponent.h"
#include "Characters/MetaPawnExtensionComponent.h"
#include "Player/MetaPlayerState.h"
#include "System/MetaGameplayTags.h"
#include "Components/GameFrameworkComponentManager.h"
#include "Player/MetaPlayerController.h"
#include "Characters/MetaPawnData.h"
#include "Player/MetaLocalPlayer.h"
#include "EnhancedInputSubsystems.h"
#include "UserSettings/EnhancedInputUserSettings.h"
#include "Input/MetaInputComponent.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "Components/GameFrameworkComponentDelegates.h"
#include "PlayerMappableInputConfig.h"
#include "InputMappingContext.h"

const FName UMetaHeroComponent::NAME_ActorFeatureName("Hero");
const FName UMetaHeroComponent::NAME_BindInputsNow("BindInputsNow");

UMetaHeroComponent::UMetaHeroComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bReadyToBindInputs(false)
{
}

bool UMetaHeroComponent::CanChangeInitState(UGameFrameworkComponentManager* Manager, FGameplayTag CurrentState, FGameplayTag DesiredState) const
{
	APawn* Pawn = GetPawn<APawn>();

	if (!CurrentState.IsValid() && DesiredState == MetaGameplayTags::InitState_Spawned)
	{
		// As long as we have a real pawn, let us transition
		if (Pawn)
		{
			return true;
		}
	}
	else if (CurrentState == MetaGameplayTags::InitState_Spawned && DesiredState == MetaGameplayTags::InitState_DataAvailable)
	{
		// The player state is required.
		if (!GetPlayerState<AMetaPlayerState>())
		{
			return false;
		}

		// If we're authority or autonomous, we need to wait for a controller with registered ownership of the player state.
		if (Pawn->GetLocalRole() != ROLE_SimulatedProxy)
		{
			AController* Controller = GetController<AController>();

			const bool bHasControllerPairedWithPS = (Controller != nullptr) && \
				(Controller->PlayerState != nullptr) && \
				(Controller->PlayerState->GetOwner() == Controller);

			if (!bHasControllerPairedWithPS)
			{
				return false;
			}
		}

		const bool bIsLocallyControlled = Pawn->IsLocallyControlled();
		const bool bIsBot = Pawn->IsBotControlled();

		if (bIsLocallyControlled && !bIsBot)
		{
			AMetaPlayerController* MetaPC = GetController<AMetaPlayerController>();

			// The input component and local player is required when locally controlled.
			if (!MetaPC || !MetaPC->GetLocalPlayer())
			{
				return false;
			}
		}

		return true;
	}
	else if (CurrentState == MetaGameplayTags::InitState_DataAvailable && DesiredState == MetaGameplayTags::InitState_DataInitialized)
	{
		// Wait for player state and extension component
		AMetaPlayerState* MetaPS = GetPlayerState<AMetaPlayerState>();

		return MetaPS && Manager->HasFeatureReachedInitState(Pawn, UMetaPawnExtensionComponent::NAME_ActorFeatureName, MetaGameplayTags::InitState_DataInitialized);
	}
	else if (CurrentState == MetaGameplayTags::InitState_DataInitialized && DesiredState == MetaGameplayTags::InitState_GameplayReady)
	{
		// TODO add ability initialization checks?
		return true;
	}

	return false;
}

void UMetaHeroComponent::HandleChangeInitState(UGameFrameworkComponentManager* Manager, FGameplayTag CurrentState, FGameplayTag DesiredState)
{
	if (CurrentState == MetaGameplayTags::InitState_DataAvailable && DesiredState == MetaGameplayTags::InitState_DataInitialized)
	{
		UE_LOG(LogTemp, Warning, TEXT("UMetaHeroComponent::HandleChangeInitState"));
		APawn* Pawn = GetPawn<APawn>();
		AMetaPlayerState* MetaPS = GetPlayerState<AMetaPlayerState>();
		if (!ensure(Pawn && MetaPS))
		{
			return;
		}

		const UMetaPawnData* PawnData = nullptr;

		if (UMetaPawnExtensionComponent* PawnExtComp = UMetaPawnExtensionComponent::FindPawnExtensionComponent(Pawn))
		{
			PawnData = PawnExtComp->GetPawnData<UMetaPawnData>();

			// The player state holds the persistent data for this player (state that persists across deaths and multiple pawns).
			// The ability system component and attribute sets live on the player state.
			PawnExtComp->InitializeAbilitySystem(MetaPS->GetMetaAbilitySystemComponent(), MetaPS);
		}
	}
}

void UMetaHeroComponent::OnActorInitStateChanged(const FActorInitStateChangedParams& Params)
{
	if (Params.FeatureName == UMetaPawnExtensionComponent::NAME_ActorFeatureName)
	{
		if (Params.FeatureState == MetaGameplayTags::InitState_DataInitialized)
		{
			// If the extension component says all all other components are initialized, try to progress to next state
			CheckDefaultInitialization();
		}
	}
}

void UMetaHeroComponent::CheckDefaultInitialization()
{
	static const TArray<FGameplayTag> StateChain = { MetaGameplayTags::InitState_Spawned, MetaGameplayTags::InitState_DataAvailable, MetaGameplayTags::InitState_DataInitialized, MetaGameplayTags::InitState_GameplayReady };

	// This will try to progress from spawned (which is only set in BeginPlay) through the data initialization stages until it gets to gameplay ready
	ContinueInitStateChain(StateChain);
}

bool UMetaHeroComponent::IsReadyToBindInputs() const
{
	return bReadyToBindInputs;
}

void UMetaHeroComponent::OnRegister()
{
	Super::OnRegister();

	if (GetPawn<APawn>())
	{
		// Register with the init state system early, this will only work if this is a game world
		RegisterInitStateFeature();
	}
}

void UMetaHeroComponent::BeginPlay()
{
	Super::BeginPlay();

	// Listen for when the pawn extension component changes init state
	BindOnActorInitStateChanged(UMetaPawnExtensionComponent::NAME_ActorFeatureName, FGameplayTag(), false);

	// Notifies that we are done spawning, then try the rest of initialization
	ensure(TryToChangeInitState(MetaGameplayTags::InitState_Spawned));
	CheckDefaultInitialization();
}

void UMetaHeroComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UnregisterInitStateFeature();

	Super::EndPlay(EndPlayReason);
}

void UMetaHeroComponent::InitializePlayerInput(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	const APawn* Pawn = GetPawn<APawn>();
	if (!Pawn)
	{
		return;
	}

	const APlayerController* PC = GetController<APlayerController>();
	check(PC);

	const UMetaLocalPlayer* LP = Cast<UMetaLocalPlayer>(PC->GetLocalPlayer());
	check(LP);

	UEnhancedInputLocalPlayerSubsystem* Subsystem = LP->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>();
	check(Subsystem);

	Subsystem->ClearAllMappings();

	if (const UMetaPawnExtensionComponent* PawnExtComp = UMetaPawnExtensionComponent::FindPawnExtensionComponent(Pawn))
	{
		if (const UMetaPawnData* PawnData = PawnExtComp->GetPawnData<UMetaPawnData>())
		{
			if (const UMetaInputConfig* InputConfig = PawnData->InputConfig)
			{
				for (const FInputMappingContextAndPriority& Mapping : DefaultInputMappings)
				{
					if (Mapping.InputMapping.IsPending())
					{
						Mapping.InputMapping.LoadSynchronous();
					}

					if (UInputMappingContext* IMC = Mapping.InputMapping.Get())
					{
						if (Mapping.bRegisterWithSettings)
						{
							if (UEnhancedInputUserSettings* Settings = Subsystem->GetUserSettings())
							{
								Settings->RegisterInputMappingContext(IMC);
							}

							FModifyContextOptions Options = {};
							Options.bIgnoreAllPressedKeysUntilRelease = false;
							// Actually add the config to the local player							
							Subsystem->AddMappingContext(IMC, Mapping.Priority, Options);
						}
					}
				}

				// The Meta Input Component has some additional functions to map Gameplay Tags to an Input Action.
				// If you want this functionality but still want to change your input component class, make it a subclass
				// of the UMetaInputComponent or modify this component accordingly.
				UMetaInputComponent* MetaIC = Cast<UMetaInputComponent>(PlayerInputComponent);
				if (ensureMsgf(MetaIC, TEXT("Unexpected Input Component class! The Gameplay Abilities will not be bound to their inputs. Change the input component to UMetaInputComponent or a subclass of it.")))
				{
					// Add the key mappings that may have been set by the player
					MetaIC->AddInputMappings(InputConfig, Subsystem);

					// This is where we actually bind and input action to a gameplay tag, which means that Gameplay Ability Blueprints will
					// be triggered directly by these input actions Triggered events. 
					TArray<uint32> BindHandles;
					MetaIC->BindAbilityActions(InputConfig, this, &ThisClass::Input_AbilityInputTagPressed, &ThisClass::Input_AbilityInputTagReleased, /*out*/ BindHandles);

					MetaIC->BindNativeAction(InputConfig, MetaGameplayTags::InputTag_Move, ETriggerEvent::Triggered, this, &ThisClass::Input_Move, /*bLogIfNotFound=*/ false);
					MetaIC->BindNativeAction(InputConfig, MetaGameplayTags::InputTag_Look_Mouse, ETriggerEvent::Triggered, this, &ThisClass::Input_LookMouse, /*bLogIfNotFound=*/ false);
				}
			}
		}
	}

	if (ensure(!bReadyToBindInputs))
	{
		bReadyToBindInputs = true;
	}

	UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(const_cast<APlayerController*>(PC), NAME_BindInputsNow);
	UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(const_cast<APawn*>(Pawn), NAME_BindInputsNow);
}

void UMetaHeroComponent::Input_AbilityInputTagPressed(FGameplayTag InputTag)
{
	if (const APawn* Pawn = GetPawn<APawn>())
	{
		if (const UMetaPawnExtensionComponent* PawnExtComp = UMetaPawnExtensionComponent::FindPawnExtensionComponent(Pawn))
		{
			if (UMetaAbilitySystemComponent* MetaASC = PawnExtComp->GetMetaAbilitySystemComponent())
			{
				MetaASC->AbilityInputTagPressed(InputTag);
			}
		}
	}
}

void UMetaHeroComponent::Input_AbilityInputTagReleased(FGameplayTag InputTag)
{
	const APawn* Pawn = GetPawn<APawn>();
	if (!Pawn)
	{
		return;
	}

	if (const UMetaPawnExtensionComponent* PawnExtComp = UMetaPawnExtensionComponent::FindPawnExtensionComponent(Pawn))
	{
		if (UMetaAbilitySystemComponent* MetaASC = PawnExtComp->GetMetaAbilitySystemComponent())
		{
			MetaASC->AbilityInputTagReleased(InputTag);
		}
	}
}

void UMetaHeroComponent::Input_Move(const FInputActionValue& InputActionValue)
{
	APawn* Pawn = GetPawn<APawn>();
	AController* Controller = Pawn ? Pawn->GetController() : nullptr;

	if (Controller)
	{
		const FVector2D Value = InputActionValue.Get<FVector2D>();
		const FRotator MovementRotation(0.0f, Controller->GetControlRotation().Yaw, 0.0f);

		if (Value.X != 0.0f)
		{
			const FVector MovementDirection = MovementRotation.RotateVector(FVector::RightVector);
			Pawn->AddMovementInput(MovementDirection, Value.X);
		}

		if (Value.Y != 0.0f)
		{
			const FVector MovementDirection = MovementRotation.RotateVector(FVector::ForwardVector);
			Pawn->AddMovementInput(MovementDirection, Value.Y);
		}
	}
}

void UMetaHeroComponent::Input_LookMouse(const FInputActionValue& InputActionValue)
{
	APawn* Pawn = GetPawn<APawn>();

	if (!Pawn)
	{
		return;
	}

	const FVector2D Value = InputActionValue.Get<FVector2D>();

	if (Value.X != 0.0f)
	{
		Pawn->AddControllerYawInput(Value.X);
	}

	if (Value.Y != 0.0f)
	{
		Pawn->AddControllerPitchInput(Value.Y);
	}
}
