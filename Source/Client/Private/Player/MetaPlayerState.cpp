// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/MetaPlayerState.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "AbilitySystem/Attributes/MetaHealthSet.h"
#include "Net/UnrealNetwork.h"
#include "Characters/MetaPawnExtensionComponent.h"
#include "Characters/MetaPawnData.h"
#include "AbilitySystem/MetaAbilitySet.h"
#include "Components/GameFrameworkComponentManager.h"
#include "System/GameMode/MetaExperienceManagerComponent.h"
#include "System/GameMode/MetaGameMode.h"
#include "AbilitySystem/Attributes/MetaCombatSet.h"

const FName AMetaPlayerState::NAME_MetaAbilityReady("MetaAbilitiesReady");

AMetaPlayerState::AMetaPlayerState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, MyPlayerConnectionType(EMetaPlayerConnectionType::Player)
{
	AbilitySystemComponent = ObjectInitializer.CreateDefaultSubobject<UMetaAbilitySystemComponent>(this, TEXT("AbilitySystemComponent"));
	if (IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->SetIsReplicated(true);
		AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);
	}

	HealthSet = CreateDefaultSubobject<UMetaHealthSet>(TEXT("HealthSet"));
	CombatSet = CreateDefaultSubobject<UMetaCombatSet>(TEXT("CombatSet"));
	NetUpdateFrequency = 100.0f;
}

void AMetaPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ThisClass, PawnData, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ThisClass, MyPlayerConnectionType, SharedParams)
}

void AMetaPlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(LogTemp, Warning, TEXT("AMetaPlayerState::PostInitializeComponents"));
	if (!IsValid(AbilitySystemComponent))
	{
		return;
	}
	AbilitySystemComponent->InitAbilityActorInfo(this, GetPawn());

	UWorld* World = GetWorld();
	if (World && World->IsGameWorld() && World->GetNetMode() != NM_Client)
	{
		AGameStateBase* GameState = GetWorld()->GetGameState();
		check(GameState);
		UMetaExperienceManagerComponent* ExperienceComponent = GameState->FindComponentByClass<UMetaExperienceManagerComponent>();
		check(ExperienceComponent);
		ExperienceComponent->CallOrRegister_OnExperienceLoaded(FOnMetaExperienceLoaded::FDelegate::CreateUObject(this, &ThisClass::OnExperienceLoaded));
	}
}

void AMetaPlayerState::ClientInitialize(AController* C)
{
	Super::ClientInitialize(C);

	if (UMetaPawnExtensionComponent* PawnExtComp = UMetaPawnExtensionComponent::FindPawnExtensionComponent(GetPawn()))
	{
		PawnExtComp->CheckDefaultInitialization();
	}
}

void AMetaPlayerState::SetPawnData(const UMetaPawnData* InPawnData)
{
	if (!IsValid(InPawnData))
	{
		return;
	}

	if (GetLocalRole() != ROLE_Authority)
	{
		return;
	}

	if (PawnData)
	{
		return;
	}

	PawnData = InPawnData;
	MARK_PROPERTY_DIRTY_FROM_NAME(ThisClass, PawnData, this);

	for (const UMetaAbilitySet* AbilitySet : PawnData->AbilitySets)
	{
		if (AbilitySet)
		{
			AbilitySet->GiveToAbilitySystem(AbilitySystemComponent, nullptr);
		}
	}

	UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(this, NAME_MetaAbilityReady);

	ForceNetUpdate();
}

void AMetaPlayerState::OnRep_PawnData()
{
}

void AMetaPlayerState::OnExperienceLoaded(const UMetaExperienceDefinition* CurrentExperience)
{
	if (AMetaGameMode* MetaGameMode = GetWorld()->GetAuthGameMode<AMetaGameMode>())
	{
		if (const UMetaPawnData* NewPawnData = MetaGameMode->GetPawnDataForController(GetOwningController()))
		{
			SetPawnData(NewPawnData);
		}
	}
}

UAbilitySystemComponent* AMetaPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}
