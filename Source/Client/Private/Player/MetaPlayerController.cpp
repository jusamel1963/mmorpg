// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/MetaPlayerController.h"
#include "AbilitySystem/MetaAbilitySystemComponent.h"
#include "Player/MetaPlayerState.h"
#include "Inventory/MetaInventoryManagerComponent.h"
#include "Equip/MetaQuickBarComponent.h"
#include "Player/MetaCheatManager.h"
#include "Characters/MetaCharacter.h"
#include "Weapon/MetaWeaponStateComponent.h"

AMetaPlayerController::AMetaPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InventoryManagerComponent = CreateDefaultSubobject<UMetaInventoryManagerComponent>(TEXT("InventoryManagerComponent"));
	QuickBarComponent = CreateDefaultSubobject<UMetaQuickBarComponent>(TEXT("QuickBarComponent"));
	MetaWeaponStateComponent = CreateDefaultSubobject<UMetaWeaponStateComponent>(TEXT("MetaWeaponStateComponent"));
	CheatClass = UMetaCheatManager::StaticClass();
}

void AMetaPlayerController::PreProcessInput(const float DeltaTime, const bool bGamePaused)
{
	Super::PreProcessInput(DeltaTime, bGamePaused);
}

void AMetaPlayerController::PostProcessInput(const float DeltaTime, const bool bGamePaused)
{
	if (UMetaAbilitySystemComponent* MetaASC = GeMetaAbilitySystemComponent())
	{
		MetaASC->ProcessAbilityInput(DeltaTime, bGamePaused);
	}

	Super::PostProcessInput(DeltaTime, bGamePaused);
}

AMetaPlayerState* AMetaPlayerController::GetMetaPlayerState() const
{
	return CastChecked<AMetaPlayerState>(PlayerState, ECastCheckedType::NullAllowed);
}

UMetaAbilitySystemComponent* AMetaPlayerController::GeMetaAbilitySystemComponent() const
{
	const AMetaPlayerState* MetaPS = GetMetaPlayerState();
	return (MetaPS ? MetaPS->GetMetaAbilitySystemComponent() : nullptr);
}

void AMetaPlayerController::GiveWeapon()
{
	if (AMetaCharacter* MetaCharacter = Cast<AMetaCharacter>(GetPawn()))
	{
		MetaCharacter->GiveInitWeapon();
	}
}