// Fill out your copyright notice in the Description page of Project Settings.


#include "Input/MetaInputComponent.h"

UMetaInputComponent::UMetaInputComponent(const FObjectInitializer& ObjectInitializer)
{
}

void UMetaInputComponent::AddInputMappings(const UMetaInputConfig* InputConfig, UEnhancedInputLocalPlayerSubsystem* InputSubsystem) const
{
	check(InputConfig);
	check(InputSubsystem);

	// Here you can handle any custom logic to add something from your input config if required
}

void UMetaInputComponent::RemoveInputMappings(const UMetaInputConfig* InputConfig, UEnhancedInputLocalPlayerSubsystem* InputSubsystem) const
{
	check(InputConfig);
	check(InputSubsystem);

	// Here you can handle any custom logic to remove input mappings that you may have added above
}

void UMetaInputComponent::RemoveBinds(TArray<uint32>& BindHandles)
{
	for (uint32 Handle : BindHandles)
	{
		RemoveBindingByHandle(Handle);
	}
	BindHandles.Reset();
}
