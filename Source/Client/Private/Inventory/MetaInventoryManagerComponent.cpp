// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/MetaInventoryManagerComponent.h"
#include "Inventory/MetaInventoryItemInstance.h"
#include "Inventory/MetaInventoryItemDefinition.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

// Sets default values for this component's properties
UMetaInventoryManagerComponent::UMetaInventoryManagerComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, InventoryList(this)
{
	bReplicateUsingRegisteredSubObjectList = true;
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	SetIsReplicatedByDefault(true);
}

UMetaInventoryItemInstance* UMetaInventoryManagerComponent::FindFirstItemStackByDefinition(TSubclassOf<UMetaInventoryItemDefinition> ItemDef) const
{
	for (const FMetaInventoryEntry& Entry : InventoryList.Entries)
	{
		UMetaInventoryItemInstance* Instance = Entry.Instance;

		if (IsValid(Instance))
		{
			if (Instance->GetItemDef() == ItemDef)
			{
				return Instance;
			}
		}
	}

	return nullptr;
}

void UMetaInventoryManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ThisClass, InventoryList);
}

UMetaInventoryItemInstance* UMetaInventoryManagerComponent::AddItemDefinition(TSubclassOf<UMetaInventoryItemDefinition> ItemDef, int32 StackCount)
{
	UMetaInventoryItemInstance* Result = nullptr;
	if (ItemDef != nullptr)
	{
		Result = InventoryList.AddEntry(ItemDef, StackCount);

		if (IsUsingRegisteredSubObjectList() && IsReadyForReplication() && Result)
		{
			AddReplicatedSubObject(Result);
		}
	}
	return Result;
}


// Called when the game starts
void UMetaInventoryManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

bool UMetaInventoryManagerComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (FMetaInventoryEntry& Entry : InventoryList.Entries)
	{
		UMetaInventoryItemInstance* Instance = Entry.Instance;

		if (Instance && IsValid(Instance))
		{
			WroteSomething |= Channel->ReplicateSubobject(Instance, *Bunch, *RepFlags);
		}
	}

	return WroteSomething;
}

void UMetaInventoryManagerComponent::ReadyForReplication()
{
	Super::ReadyForReplication();

	// Register existing UMetaInventoryItemInstance
	if (IsUsingRegisteredSubObjectList())
	{
		for (const FMetaInventoryEntry& Entry : InventoryList.Entries)
		{
			UMetaInventoryItemInstance* Instance = Entry.Instance;

			if (IsValid(Instance))
			{
				AddReplicatedSubObject(Instance);
			}
		}
	}
}


// Called every frame
void UMetaInventoryManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FString FMetaInventoryEntry::GetDebugString() const
{
	return FString();
}

TArray<UMetaInventoryItemInstance*> FMetaInventoryList::GetAllItems() const
{
	return TArray<UMetaInventoryItemInstance*>();
}

void FMetaInventoryList::PreReplicatedRemove(const TArrayView<int32> RemovedIndices, int32 FinalSize)
{
}

void FMetaInventoryList::PostReplicatedAdd(const TArrayView<int32> AddedIndices, int32 FinalSize)
{
}

void FMetaInventoryList::PostReplicatedChange(const TArrayView<int32> ChangedIndices, int32 FinalSize)
{
}

UMetaInventoryItemInstance* FMetaInventoryList::AddEntry(TSubclassOf<UMetaInventoryItemDefinition> ItemClass, int32 StackCount)
{
	UMetaInventoryItemInstance* Result = nullptr;

	check(ItemClass != nullptr);
	check(OwnerComponent);

	AActor* OwningActor = OwnerComponent->GetOwner();
	check(OwningActor->HasAuthority());


	FMetaInventoryEntry& NewEntry = Entries.AddDefaulted_GetRef();
	NewEntry.Instance = NewObject<UMetaInventoryItemInstance>(OwnerComponent->GetOwner());  //@TODO: Using the actor instead of component as the outer due to UE-127172
	NewEntry.Instance->SetItemDef(ItemClass);
	for (UMetaInventoryItemFragment* Fragment : GetDefault<UMetaInventoryItemDefinition>(ItemClass)->Fragments)
	{
		if (Fragment != nullptr)
		{
			Fragment->OnInstanceCreated(NewEntry.Instance);
		}
	}
	NewEntry.StackCount = StackCount;
	Result = NewEntry.Instance;

	MarkItemDirty(NewEntry);

	return Result;
}

void FMetaInventoryList::AddEntry(UMetaInventoryItemInstance* Instance)
{
}

void FMetaInventoryList::RemoveEntry(UMetaInventoryItemInstance* Instance)
{
}

void FMetaInventoryList::BroadcastChangeMessage(FMetaInventoryEntry& Entry, int32 OldCount, int32 NewCount)
{
}
