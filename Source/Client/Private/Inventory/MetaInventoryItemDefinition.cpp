// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/MetaInventoryItemDefinition.h"

const UMetaInventoryItemFragment* UMetaInventoryItemDefinition::FindFragmentByClass(TSubclassOf<UMetaInventoryItemFragment> FragmentClass) const
{
	if (FragmentClass != nullptr)
	{
		for (UMetaInventoryItemFragment* Fragment : Fragments)
		{
			if (Fragment && Fragment->IsA(FragmentClass))
			{
				return Fragment;
			}
		}
	}

	return nullptr;
}
