// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/MetaInventoryItemInstance.h"
#include "Net/UnrealNetwork.h"
#include "Inventory/MetaInventoryItemDefinition.h"

void UMetaInventoryItemInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, ItemDef);
}

const UMetaInventoryItemFragment* UMetaInventoryItemInstance::FindFragmentByClass(TSubclassOf<UMetaInventoryItemFragment> FragmentClass) const
{
	if ((ItemDef != nullptr) && (FragmentClass != nullptr))
	{
		return GetDefault<UMetaInventoryItemDefinition>(ItemDef)->FindFragmentByClass(FragmentClass);
	}

	return nullptr;
}

void UMetaInventoryItemInstance::SetItemDef(TSubclassOf<UMetaInventoryItemDefinition> InDef)
{
	ItemDef = InDef;
}
