// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/MetaWeaponInstance.h"
#include "Engine/World.h"

void UMetaWeaponInstance::UpdateFiringTime()
{
	UWorld* World = GetWorld();
	check(World);
	TimeLastFired = World->GetTimeSeconds();
}
