// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/MetaWeaponSpawner.h"

// Sets default values
AMetaWeaponSpawner::AMetaWeaponSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMetaWeaponSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMetaWeaponSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

