// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/MetaWeaponBase.h"

// Sets default values
AMetaWeaponBase::AMetaWeaponBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	bReplicateUsingRegisteredSubObjectList = true;
}

// Called when the game starts or when spawned
void AMetaWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMetaWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

