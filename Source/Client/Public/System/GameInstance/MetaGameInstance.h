// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "System/GameInstance/CommonGameInstance.h"
#include "MetaGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API UMetaGameInstance : public UCommonGameInstance
{
	GENERATED_BODY()

protected:
	virtual void Init() override;
};
