// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "MetaAssetManager.generated.h"

class UMetaPawnData;

/**
 * UMetaAssetManager
 *
 *	Game implementation of the asset manager that overrides functionality and stores game-specific types.
 *	It is expected that most games will want to override AssetManager as it provides a good place for game-specific loading logic.
 *	This class is used by setting 'AssetManagerClassName' in DefaultEngine.ini.
 */
UCLASS(Config = Game)
class CLIENT_API UMetaAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:
	UMetaAssetManager();

	// Returns the AssetManager singleton object.
	static UMetaAssetManager& Get();

protected:
	// Pawn data used when spawning player pawns if there isn't one set on the player state.
	UPROPERTY(Config)
	TSoftObjectPtr<UMetaPawnData> DefaultPawnData;
};
