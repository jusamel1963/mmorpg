// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ModularGameState.h"
#include "AbilitySystemInterface.h"
#include "MetaGameState.generated.h"

class UMetaAbilitySystemComponent;
class UMetaExperienceManagerComponent;

UCLASS()
class CLIENT_API AMetaGameState : public AModularGameStateBase, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AMetaGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//~AActor interface
	virtual void PreInitializeComponents() override;
	virtual void PostInitializeComponents() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaSeconds) override;
	//~End of AActor interface

	// Gets the ability system component used for game wide things
	UFUNCTION(BlueprintCallable, Category = "Meta|GameState")
	UMetaAbilitySystemComponent* GetMetaAbilitySystemComponent() const { return AbilitySystemComponent; }

	//~IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	//~End of IAbilitySystemInterface


private:
	// Handles loading and managing the current gameplay experience
	UPROPERTY()
	TObjectPtr<UMetaExperienceManagerComponent> ExperienceManagerComponent;

	// The ability system component subobject for game-wide things (primarily gameplay cues)
	UPROPERTY(VisibleAnywhere, Category = "Meta|GameState")
	TObjectPtr<UMetaAbilitySystemComponent> AbilitySystemComponent;
};
