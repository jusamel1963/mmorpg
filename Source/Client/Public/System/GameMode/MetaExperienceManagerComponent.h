// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/GameStateComponent.h"
#include "Loading/LoadingProcessInterface.h"
#include "MetaExperienceManagerComponent.generated.h"

class UMetaExperienceDefinition;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMetaExperienceLoaded, const UMetaExperienceDefinition* /*Experience*/);

enum class EMetaExperienceLoadState
{
	Unloaded,
	Loading,
	LoadingGameFeatures,
	LoadingChaosTestingDelay,
	ExecutingActions,
	Loaded,
	Deactivating
};

UCLASS()
class CLIENT_API UMetaExperienceManagerComponent : public UGameStateComponent, public ILoadingProcessInterface
{
	GENERATED_BODY()

public:
	// Ensures the delegate is called once the experience has been loaded
	// If the experience has already loaded, calls the delegate immediately
	void CallOrRegister_OnExperienceLoaded(FOnMetaExperienceLoaded::FDelegate&& Delegate);

	// Returns true if the experience is fully loaded
	bool IsExperienceLoaded() const;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	// This returns the current experience if it is fully loaded, asserting otherwise
	// (i.e., if you called it too soon)
	const UMetaExperienceDefinition* GetCurrentExperienceChecked() const;
	// Tries to set the current experience, either a UI or gameplay one
	void SetCurrentExperience(FPrimaryAssetId ExperienceId);
private:
	UFUNCTION()
	void OnRep_CurrentExperience();
	void StartExperienceLoad();
	void OnExperienceFullLoadCompleted();

private:
	UPROPERTY(ReplicatedUsing = OnRep_CurrentExperience)
	TObjectPtr<const UMetaExperienceDefinition> CurrentExperience;

	EMetaExperienceLoadState LoadState = EMetaExperienceLoadState::Unloaded;

	/**
	* Delegate called when the experience has finished loading just before others
	* (e.g., subsystems that set up for regular gameplay)
	*/
	FOnMetaExperienceLoaded OnExperienceLoaded_HighPriority;

	/** Delegate called when the experience has finished loading */
	FOnMetaExperienceLoaded OnExperienceLoaded;

	/** Delegate called when the experience has finished loading */
	FOnMetaExperienceLoaded OnExperienceLoaded_LowPriority;
};
