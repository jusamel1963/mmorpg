// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MetaExperienceDefinition.generated.h"

class UMetaPawnData;

UCLASS()
class CLIENT_API UMetaExperienceDefinition : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	//@TODO: Make soft?
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TObjectPtr<const UMetaPawnData> DefaultPawnData;
};
