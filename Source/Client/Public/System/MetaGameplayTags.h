#pragma once

#include "NativeGameplayTags.h"

namespace MetaGameplayTags
{
	CLIENT_API	FGameplayTag FindTagByString(const FString& TagString, bool bMatchPartialString = false);

	// Declare all of the custom native tags that Meta will use

	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InitState_Spawned);
	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InitState_DataAvailable);
	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InitState_DataInitialized);
	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InitState_GameplayReady);
	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(Ability_Behavior_SurvivesDeath);

	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InputTag_Move);
	CLIENT_API	UE_DECLARE_GAMEPLAY_TAG_EXTERN(InputTag_Look_Mouse);
};
