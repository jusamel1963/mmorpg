// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/GameFrameworkComponent.h"
#include "MetaHealthComponent.generated.h"

class UMetaAbilitySystemComponent;
class UMetaHealthSet;
class AActor;
class UMetaHealthComponent;
struct FGameplayEffectSpec;

UENUM(BlueprintType)
enum class EMetaDeathState : uint8
{
	NotDead = 0,
	DeathStarted,
	DeathFinished
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMetaHealth_DeathEvent, AActor*, OwningActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FMetaHealth_AttributeChanged, UMetaHealthComponent*, HealthComponent, float, OldValue, float, NewValue, AActor*, Instigator);

UCLASS(Blueprintable, Meta = (BlueprintSpawnableComponent))
class CLIENT_API UMetaHealthComponent : public UGameFrameworkComponent
{
	GENERATED_BODY()

public:
	UMetaHealthComponent(const FObjectInitializer& ObjectInitializer);
	// Initialize the component using an ability system component.
	UFUNCTION(BlueprintCallable, Category = "Meta|Health")
	void InitializeWithAbilitySystem(UMetaAbilitySystemComponent* InASC);

	// Uninitialize the component, clearing any references to the ability system.
	UFUNCTION(BlueprintCallable, Category = "Meta|Health")
	void UninitializeFromAbilitySystem();

	// Applies enough damage to kill the owner.
	virtual void DamageSelfDestruct(bool bFellOutOfWorld = false);
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	// Returns the current health value.
	UFUNCTION(BlueprintCallable, Category = "Meta|Health")
	float GetHealth() const;

	// Returns the current maximum health value.
	UFUNCTION(BlueprintCallable, Category = "Meta|Health")
	float GetMaxHealth() const;
protected:
	UFUNCTION()
	virtual void OnRep_DeathState(EMetaDeathState OldDeathState);

	virtual void HandleHealthChanged(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec* DamageEffectSpec, float DamageMagnitude, float OldValue, float NewValue);
	virtual void HandleMaxHealthChanged(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec* DamageEffectSpec, float DamageMagnitude, float OldValue, float NewValue);
	virtual void HandleOutOfHealth(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec* DamageEffectSpec, float DamageMagnitude, float OldValue, float NewValue);

	void ClearGameplayTags();
public:
	// Delegate fired when the death sequence has started.
	UPROPERTY(BlueprintAssignable)
	FMetaHealth_DeathEvent OnDeathStarted;

	// Delegate fired when the death sequence has finished.
	UPROPERTY(BlueprintAssignable)
	FMetaHealth_DeathEvent OnDeathFinished;

	// Delegate fired when the health value has changed. This is called on the client but the instigator may not be valid
	UPROPERTY(BlueprintAssignable)
	FMetaHealth_AttributeChanged OnHealthChanged;

	// Delegate fired when the max health value has changed. This is called on the client but the instigator may not be valid
	UPROPERTY(BlueprintAssignable)
	FMetaHealth_AttributeChanged OnMaxHealthChanged;
protected:
	// Replicated state used to handle dying.
	UPROPERTY(ReplicatedUsing = OnRep_DeathState)
	EMetaDeathState DeathState;

	// Ability system used by this component.
	UPROPERTY()
	TObjectPtr<UMetaAbilitySystemComponent> AbilitySystemComponent;

	// Health set used by this component.
	UPROPERTY()
	TObjectPtr<const UMetaHealthSet> HealthSet;

};
