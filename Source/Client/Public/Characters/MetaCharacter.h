// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ModularCharacter.h"
#include "GameplayCueInterface.h"
#include "GameplayTagAssetInterface.h"
#include "AbilitySystemInterface.h"
#include "MetaCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;
class UMetaPawnExtensionComponent;
class UMetaHeroComponent;
class UMetaHealthComponent;
class UMetaAbilitySystemComponent;
class UMetaEquipManagerComponent;
class UMetaPickupDefinition;

UCLASS(Config = Game)
class CLIENT_API AMetaCharacter : public AModularCharacter, public IAbilitySystemInterface, public IGameplayCueInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()

public:
	AMetaCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;
	virtual bool HasMatchingGameplayTag(FGameplayTag TagToCheck) const override;
	virtual bool HasAllMatchingGameplayTags(const FGameplayTagContainer& TagContainer) const override;
	virtual bool HasAnyMatchingGameplayTags(const FGameplayTagContainer& TagContainer) const override;

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	UMetaAbilitySystemComponent* GetMetaAbilitySystemComponent() const;

	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;
	UFUNCTION(Server, Reliable)
	void GiveInitWeapon();
protected:

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// To add mapping context
	virtual void BeginPlay();
	virtual void OnAbilitySystemInitialized();
	virtual void OnAbilitySystemUninitialized();
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;

	virtual void OnRep_Controller() override;
	virtual void OnRep_PlayerState() override;
	// Begins the death sequence for the character (disables collision, disables movement, etc...)
	UFUNCTION()
	virtual void OnDeathStarted(AActor* OwningActor);

	// Ends the death sequence for the character (detaches controller, destroys pawn, etc...)
	UFUNCTION()
	virtual void OnDeathFinished(AActor* OwningActor);
public:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Character", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaPawnExtensionComponent> PawnExtComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Character", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaHeroComponent> MetaHeroComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Character", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaHealthComponent> HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Character", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaEquipManagerComponent> EquipManagerComponent;

	UPROPERTY(EditAnywhere, Category = "Meta|Character", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaPickupDefinition> InitPickupDefinition;
};
