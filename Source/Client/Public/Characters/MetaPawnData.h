// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MetaPawnData.generated.h"

class UMetaAbilitySet;
class UMetaTagRelationshipMapping;
class UMetaInputConfig;

UCLASS()
class CLIENT_API UMetaPawnData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UMetaPawnData(const FObjectInitializer& ObjectInitializer);

public:

	// Class to instantiate for this pawn (should usually derive from AMetaPawn or AMetaCharacter).
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pawn")
	TSubclassOf<APawn> PawnClass;

	// Ability sets to grant to this pawn's ability system.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Abilities")
	TArray<TObjectPtr<UMetaAbilitySet>> AbilitySets;

	// What mapping of ability tags to use for actions taking by this pawn
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Abilities")
	TObjectPtr<UMetaTagRelationshipMapping> TagRelationshipMapping;

	//Input configuration used by player controlled pawns to create input mappings and bind input actions.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Input")
	TObjectPtr<UMetaInputConfig> InputConfig;
};
