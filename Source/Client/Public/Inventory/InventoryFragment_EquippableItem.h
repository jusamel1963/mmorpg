// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Inventory/MetaInventoryItemDefinition.h"
#include "InventoryFragment_EquippableItem.generated.h"

class UMetaEquipmentDefinition;

UCLASS()
class CLIENT_API UInventoryFragment_EquippableItem : public UMetaInventoryItemFragment
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = Meta)
	TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition;
};
