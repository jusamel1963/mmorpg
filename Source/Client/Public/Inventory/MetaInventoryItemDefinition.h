// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MetaInventoryItemDefinition.generated.h"

class UMetaInventoryItemInstance;
class UMetaInventoryItemFragment;

//////////////////////////////////////////////////////////////////////
// Represents a fragment of an item definition
UCLASS(DefaultToInstanced, EditInlineNew, Abstract)
class CLIENT_API UMetaInventoryItemFragment : public UObject
{
	GENERATED_BODY()

public:
	virtual void OnInstanceCreated(UMetaInventoryItemInstance* Instance) const {}
};

//////////////////////////////////////////////////////////////////////


UCLASS(Blueprintable)
class CLIENT_API UMetaInventoryItemDefinition : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Display, Instanced)
	TArray<TObjectPtr<UMetaInventoryItemFragment>> Fragments;

public:
	const UMetaInventoryItemFragment* FindFragmentByClass(TSubclassOf<UMetaInventoryItemFragment> FragmentClass) const;
};
