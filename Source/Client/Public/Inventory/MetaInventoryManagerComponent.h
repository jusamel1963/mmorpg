// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Net/Serialization/FastArraySerializer.h"
#include "MetaInventoryManagerComponent.generated.h"

class UMetaInventoryItemInstance;
class UMetaInventoryItemDefinition;
struct FNetDeltaSerializeInfo;


/** A message when an item is added to the inventory */
USTRUCT(BlueprintType)
struct FMetaInventoryChangeMessage
{
	GENERATED_BODY()

	//@TODO: Tag based names+owning actors for inventories instead of directly exposing the component?
	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	TObjectPtr<UActorComponent> InventoryOwner = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	TObjectPtr<UMetaInventoryItemInstance> Instance = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	int32 NewCount = 0;

	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	int32 Delta = 0;
};

/** A single entry in an inventory */
USTRUCT(BlueprintType)
struct FMetaInventoryEntry : public FFastArraySerializerItem
{
	GENERATED_BODY()

	FMetaInventoryEntry()
	{}

	FString GetDebugString() const;

private:
	friend FMetaInventoryList;
	friend UMetaInventoryManagerComponent;

	UPROPERTY()
	TObjectPtr<UMetaInventoryItemInstance> Instance = nullptr;

	UPROPERTY()
	int32 StackCount = 0;

	UPROPERTY(NotReplicated)
	int32 LastObservedCount = INDEX_NONE;
};

/** List of inventory items */
USTRUCT(BlueprintType)
struct FMetaInventoryList : public FFastArraySerializer
{
	GENERATED_BODY()

	FMetaInventoryList()
		: OwnerComponent(nullptr)
	{
	}

	FMetaInventoryList(UActorComponent* InOwnerComponent)
		: OwnerComponent(InOwnerComponent)
	{
	}

	TArray<UMetaInventoryItemInstance*> GetAllItems() const;

public:
	//~FFastArraySerializer contract
	void PreReplicatedRemove(const TArrayView<int32> RemovedIndices, int32 FinalSize);
	void PostReplicatedAdd(const TArrayView<int32> AddedIndices, int32 FinalSize);
	void PostReplicatedChange(const TArrayView<int32> ChangedIndices, int32 FinalSize);
	//~End of FFastArraySerializer contract

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FMetaInventoryEntry, FMetaInventoryList>(Entries, DeltaParms, *this);
	}

	UMetaInventoryItemInstance* AddEntry(TSubclassOf<UMetaInventoryItemDefinition> ItemClass, int32 StackCount);
	void AddEntry(UMetaInventoryItemInstance* Instance);

	void RemoveEntry(UMetaInventoryItemInstance* Instance);

private:
	void BroadcastChangeMessage(FMetaInventoryEntry& Entry, int32 OldCount, int32 NewCount);

private:
	friend UMetaInventoryManagerComponent;

private:
	// Replicated list of items
	UPROPERTY()
	TArray<FMetaInventoryEntry> Entries;

	UPROPERTY(NotReplicated)
	TObjectPtr<UActorComponent> OwnerComponent;
};

template<>
struct TStructOpsTypeTraits<FMetaInventoryList> : public TStructOpsTypeTraitsBase2<FMetaInventoryList>
{
	enum { WithNetDeltaSerializer = true };
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CLIENT_API UMetaInventoryManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMetaInventoryManagerComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, Category = Inventory, BlueprintPure)
	UMetaInventoryItemInstance* FindFirstItemStackByDefinition(TSubclassOf<UMetaInventoryItemDefinition> ItemDef) const;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	UMetaInventoryItemInstance* AddItemDefinition(TSubclassOf<UMetaInventoryItemDefinition> ItemDef, int32 StackCount = 1);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//~UObject interface
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void ReadyForReplication() override;
	//~End of UObject interface
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(Replicated)
	FMetaInventoryList InventoryList;
		
};
