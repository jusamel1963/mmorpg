// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MetaInventoryItemInstance.generated.h"

class UMetaInventoryItemDefinition;
class UMetaInventoryItemFragment;

UCLASS()
class CLIENT_API UMetaInventoryItemInstance : public UObject
{
	GENERATED_BODY()
public:
	TSubclassOf<UMetaInventoryItemDefinition> GetItemDef() const
	{
		return ItemDef;
	}
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, BlueprintPure = false, meta = (DeterminesOutputType = FragmentClass))
	const UMetaInventoryItemFragment* FindFragmentByClass(TSubclassOf<UMetaInventoryItemFragment> FragmentClass) const;

	template <typename ResultClass>
	const ResultClass* FindFragmentByClass() const
	{
		return (ResultClass*)FindFragmentByClass(ResultClass::StaticClass());
	}

	void SetItemDef(TSubclassOf<UMetaInventoryItemDefinition> InDef);

	//~UObject interface
	virtual bool IsSupportedForNetworking() const override { return true; }
	//~End of UObject interface

private:
	// The item definition
	UPROPERTY(Replicated)
	TSubclassOf<UMetaInventoryItemDefinition> ItemDef;
};
