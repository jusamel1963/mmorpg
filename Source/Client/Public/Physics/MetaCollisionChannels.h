// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Trace against Actors/Components which provide interactions.
#define Meta_TraceChannel_Interaction					ECC_GameTraceChannel1

// Trace used by weapons, will hit physics assets instead of capsules
#define Meta_TraceChannel_Weapon						ECC_GameTraceChannel2