// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "MetaTeamSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API UMetaTeamSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()
public:
	// Returns true if the instigator can damage the target, taking into account the friendly fire settings
	bool CanCauseDamage(const UObject* Instigator, const UObject* Target, bool bAllowDamageToSelf = true) const;
};
