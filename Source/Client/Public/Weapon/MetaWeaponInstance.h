// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equip/MetaEquipmentInstance.h"
#include "MetaWeaponInstance.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API UMetaWeaponInstance : public UMetaEquipmentInstance
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	void UpdateFiringTime();

private:
	double TimeLastFired = 0.0;
};
