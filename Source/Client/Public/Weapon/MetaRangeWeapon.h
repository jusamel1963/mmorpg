// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/MetaWeaponBase.h"
#include "MetaRangeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API AMetaRangeWeapon : public AMetaWeaponBase
{
	GENERATED_BODY()

public:
	AMetaRangeWeapon(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
};
