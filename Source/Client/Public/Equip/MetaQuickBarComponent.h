// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ControllerComponent.h"
#include "MetaQuickBarComponent.generated.h"

class UMetaInventoryItemInstance;
class UMetaEquipmentInstance;
class UMetaEquipManagerComponent;

USTRUCT(BlueprintType)
struct FMetaQuickBarSlotsChangedMessage
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	TObjectPtr<AActor> Owner = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Inventory)
	TArray<TObjectPtr<UMetaInventoryItemInstance>> Slots;
};

UCLASS()
class CLIENT_API UMetaQuickBarComponent : public UControllerComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintPure = false)
	int32 GetNextFreeItemSlot() const;

	UFUNCTION(BlueprintCallable)
	void AddItemToSlot(int32 SlotIndex, UMetaInventoryItemInstance* Item);
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Meta")
	void SetActiveSlotIndex(int32 NewIndex);

	virtual void BeginPlay() override;

protected:
	UFUNCTION()
	void OnRep_Slots();
	UFUNCTION()
	void OnRep_ActiveSlotIndex();

private:
	void UnequipItemInSlot();
	void EquipItemInSlot();
	UMetaEquipManagerComponent* FindEquipmentManager() const;

protected:
	UPROPERTY()
	int32 NumSlots = 3;

private:
	UPROPERTY(ReplicatedUsing = OnRep_Slots)
	TArray<TObjectPtr<UMetaInventoryItemInstance>> Slots;

	UPROPERTY(ReplicatedUsing = OnRep_ActiveSlotIndex)
	int32 ActiveSlotIndex = -1;

	UPROPERTY()
	TObjectPtr<UMetaEquipmentInstance> EquippedItem;
};
