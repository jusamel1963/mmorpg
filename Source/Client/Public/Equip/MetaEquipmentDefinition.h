// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MetaEquipmentDefinition.generated.h"

class UMetaAbilitySet;
class UMetaEquipmentInstance;

USTRUCT()
struct FMetaEquipmentActorToSpawn
{
	GENERATED_BODY()

	FMetaEquipmentActorToSpawn()
	{}

	UPROPERTY(EditAnywhere, Category = Equipment)
	TSubclassOf<AActor> ActorToSpawn;

	UPROPERTY(EditAnywhere, Category = Equipment)
	FName AttachSocket;

	UPROPERTY(EditAnywhere, Category = Equipment)
	FTransform AttachTransform;
};

/**
 * UMetaEquipmentDefinition
 *
 * Definition of a piece of equipment that can be applied to a pawn
 */
UCLASS(Blueprintable, BlueprintType)
class CLIENT_API UMetaEquipmentDefinition : public UObject
{
	GENERATED_BODY()
public:
	UMetaEquipmentDefinition(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// Class to spawnUInventoryFragment_EquippableItem
	UPROPERTY(EditDefaultsOnly, Category = Equipment)
	TSubclassOf<UMetaEquipmentInstance> InstanceType;

	// Gameplay ability sets to grant when this is equipped
	UPROPERTY(EditDefaultsOnly, Category = Equipment)
	TArray<TObjectPtr<const UMetaAbilitySet>> AbilitySetsToGrant;

	// Actors to spawn on the pawn when this is equipped
	UPROPERTY(EditDefaultsOnly, Category = Equipment)
	TArray<FMetaEquipmentActorToSpawn> ActorsToSpawn;
};
