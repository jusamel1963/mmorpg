// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MetaPickupDefinition.generated.h"

class UMetaInventoryItemDefinition;

UCLASS(Blueprintable, BlueprintType, Const, Meta = (DisplayName = "Meta Pickup Data", ShortTooltip = "Data asset used to configure a pickup."))
class CLIENT_API UMetaPickupDefinition : public UDataAsset
{
	GENERATED_BODY()
public:

	//Defines the pickup's actors to spawn, abilities to grant, and tags to add
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup|Equipment")
	TSubclassOf<UMetaInventoryItemDefinition> InventoryItemDefinition;

	//Visual representation of the pickup
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup|Mesh")
	TObjectPtr<UStaticMesh> DisplayMesh;

	//Cool down time between pickups in seconds
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup")
	int32 SpawnCoolDownSeconds;

	//Sound to play when picked up
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup")
	TObjectPtr<USoundBase> PickedUpSound;

	//Sound to play when pickup is respawned
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup")
	TObjectPtr<USoundBase> RespawnedSound;

};

UCLASS(Blueprintable, BlueprintType, Const, Meta = (DisplayName = "Meta Weapon Pickup Data", ShortTooltip = "Data asset used to configure a weapon pickup."))
class CLIENT_API UMetaWeaponPickupDefinition : public UMetaPickupDefinition
{
	GENERATED_BODY()

public:

	//Sets the height of the display mesh above the Weapon spawner
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup|Mesh")
	FVector WeaponMeshOffset;

	//Sets the height of the display mesh above the Weapon spawner
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Meta|Pickup|Mesh")
	FVector WeaponMeshScale = FVector(1.0f, 1.0f, 1.0f);
};
