// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PawnComponent.h"
#include "AbilitySystem/MetaAbilitySet.h"
#include "Net/Serialization/FastArraySerializer.h"
#include "MetaEquipManagerComponent.generated.h"

class UMetaEquipmentInstance;
class UMetaEquipmentDefinition;
class UMetaEquipManagerComponent;
class UMetaAbilitySystemComponent;

/** A single piece of applied equipment */
USTRUCT(BlueprintType)
struct FMetaAppliedEquipmentEntry : public FFastArraySerializerItem
{
	GENERATED_BODY()

	FMetaAppliedEquipmentEntry()
	{}

	FString GetDebugString() const;

private:
	friend FMetaEquipmentList;
	friend UMetaEquipManagerComponent;

	// The equipment class that got equipped
	UPROPERTY()
	TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition;

	UPROPERTY()
	TObjectPtr<UMetaEquipmentInstance> Instance = nullptr;

	// Authority-only list of granted handles
	UPROPERTY(NotReplicated)
	FMetaAbilitySet_GrantedHandles GrantedHandles;
};

/** List of applied equipment */
USTRUCT(BlueprintType)
struct FMetaEquipmentList : public FFastArraySerializer
{
	GENERATED_BODY()

	FMetaEquipmentList()
		: OwnerComponent(nullptr)
	{
	}

	FMetaEquipmentList(UActorComponent* InOwnerComponent)
		: OwnerComponent(InOwnerComponent)
	{
	}

public:
	//~FFastArraySerializer contract
	//void PreReplicatedRemove(const TArrayView<int32> RemovedIndices, int32 FinalSize);
	//void PostReplicatedAdd(const TArrayView<int32> AddedIndices, int32 FinalSize);
	//void PostReplicatedChange(const TArrayView<int32> ChangedIndices, int32 FinalSize);
	//~End of FFastArraySerializer contract

	//bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	//{
	//	return FFastArraySerializer::FastArrayDeltaSerialize<FMetaAppliedEquipmentEntry, FMetaEquipmentList>(Entries, DeltaParms, *this);
	//}

	UMetaEquipmentInstance* AddEntry(TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition);
	void RemoveEntry(UMetaEquipmentInstance* Instance);

private:
	UMetaAbilitySystemComponent* GetAbilitySystemComponent() const;

	friend UMetaEquipManagerComponent;

private:
	// Replicated list of equipment entries
	UPROPERTY()
	TArray<FMetaAppliedEquipmentEntry> Entries;

	UPROPERTY(NotReplicated)
	TObjectPtr<UActorComponent> OwnerComponent;
};


UCLASS(BlueprintType, Const)
class CLIENT_API UMetaEquipManagerComponent : public UPawnComponent
{
	GENERATED_BODY()
public:
	UMetaEquipManagerComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable)
	UMetaEquipmentInstance* EquipItem(TSubclassOf<UMetaEquipmentDefinition> EquipmentDefinition);

	UFUNCTION(BlueprintCallable)
	void UnequipItem(UMetaEquipmentInstance* ItemInstance);

	//virtual void EndPlay() override;
	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;
	virtual void ReadyForReplication() override;
	//~End of UActorComponent interface
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//~UObject interface
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	//~End of UObject interface

	/** Returns the first equipped instance of a given type, or nullptr if none are found */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UMetaEquipmentInstance* GetFirstInstanceOfType(TSubclassOf<UMetaEquipmentInstance> InstanceType);
private:
	UPROPERTY(Replicated)
	FMetaEquipmentList EquipmentList;
};
