// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/MetaGameplayAbility.h"
#include "MetaGA_Jump.generated.h"


struct FGameplayAbilityActorInfo;
struct FGameplayTagContainer;

UCLASS(Abstract)
class CLIENT_API UMetaGA_Jump : public UMetaGameplayAbility
{
	GENERATED_BODY()
public:
	UMetaGA_Jump(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	void CharacterJumpStart();

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	void CharacterJumpStop();

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	void OnJumpingEnded();

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	void OnJumpingInterrupted();

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	void HandleInputRelease(float TimeHeld);
};
