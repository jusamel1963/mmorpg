// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/MetaGA_RangedWeapon.h"
#include "MetaGA_WeaponFire.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API UMetaGA_WeaponFire : public UMetaGA_RangedWeapon
{
	GENERATED_BODY()
protected:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void OnRangedWeaponTargetDataReady(const FGameplayAbilityTargetDataHandle& TargetData) override;
	UFUNCTION()
	void OnAMCompleted();
	UFUNCTION()
	void OnAMInterrupted();
	UFUNCTION()
	void OnAMCancelled();
protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGameplayEffect> GE_Damage;
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<UAnimMontage> AM_CharacterFire;
};
