// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "MetaGameplayAbility.generated.h"

class IMetaAbilitySourceInterface;

UENUM(BlueprintType)
enum class EMetaAbilityActivationPolicy : uint8
{
	// Try to activate the ability when the input is triggered.
	OnInputTriggered,

	// Continually try to activate the ability while the input is active.
	WhileInputActive,

	// Try to activate the ability when an avatar is assigned.
	OnSpawn
};

UCLASS()
class CLIENT_API UMetaGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UMetaGameplayAbility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	AMetaCharacter* GetMetaCharacterFromActorInfo() const;

	EMetaAbilityActivationPolicy GetActivationPolicy() const { return ActivationPolicy; }
	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	AController* GetControllerFromActorInfo() const;

protected:
	virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
	UFUNCTION(BlueprintCallable, Category = Ability, DisplayName = "OnAbilityAdded")
	void OnAbilityAdded();
	virtual FGameplayEffectContextHandle MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const override;
	virtual void GetAbilitySource(FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, float& OutSourceLevel, const IMetaAbilitySourceInterface*& OutAbilitySource, AActor*& OutEffectCauser) const;
protected:
	EMetaAbilityActivationPolicy ActivationPolicy;
};
