// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/MetaGameplayAbility.h"
#include "MetaGA_FromEquipment.generated.h"

class UMetaEquipmentInstance;

UCLASS()
class CLIENT_API UMetaGA_FromEquipment : public UMetaGameplayAbility
{
	GENERATED_BODY()

public:
	UMetaGA_FromEquipment(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, Category = "Meta|Ability")
	UMetaEquipmentInstance* GetAssociatedEquipment() const;
};
