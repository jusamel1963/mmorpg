// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "MetaAttributeSet.generated.h"

class AActor;
class UMetaAbilitySystemComponent;
class UObject;
class UWorld;
struct FGameplayEffectSpec;

/**
 * This macro defines a set of helper functions for accessing and initializing attributes.
 *
 * The following example of the macro:
 *		ATTRIBUTE_ACCESSORS(UMetaHealthSet, Health)
 * will create the following functions:
 *		static FGameplayAttribute GetHealthAttribute();
 *		float GetHealth() const;
 *		void SetHealth(float NewVal);
 *		void InitHealth(float NewVal);
 */
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)


//#define META_IMPL_REPL_ATTRIBUTE(Type, PropertyName)\
//void Type::OnRep_##PropertyName(const FGameplayAttributeData& Old##PropertyName)\
//{\
//	GAMEPLAYATTRIBUTE_REPNOTIFY(Type, PropertyName, Old##PropertyName);\
//}

//#define META_ADD_ATTRIBUTE_CHANGE_CALLBACK(Type, AttributeName, Owner, TargetObject, TargetFunction)																		\
//if(IsValid(Owner))																																							\
//{																																											\
//	UAbilitySystemComponent* __AbilitySystemComponent = Owner->GetAbilitySystemComponent();																					\
//	const Type* __METAAttributeSet = Owner->GetAttributeSet<Type>();																										\
//	if (IsValid(__AbilitySystemComponent) && IsValid(__METAAttributeSet))																									\
//	{																																										\
//		__AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(__METAAttributeSet->Get##AttributeName##Attribute()).AddUObject(TargetObject, TargetFunction);	\
//	}																																										\
//}
//
//#define META_REMOVE_ALL_ATTRIBUTE_CHANGE_CALLBACKS(AttributeName, Owner, TargetObject)																						\
//if(IsValid(Owner))																																							\
//{																																											\
//	UAbilitySystemComponent* __AbilitySystemComponent = Owner->GetAbilitySystemComponent();																					\
//	const Type* __METAAttributeSet = Owner->GetAttributeSet<Type>();																										\
//	if (IsValid(__AbilitySystemComponent) && IsValid(__METAAttributeSet))																									\
//	{																																										\
//		__AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(__METAAttributeSet->Get##AttributeName##Attribute()).RemoveAll(TargetObject);						\
//	}																																										\
//}


/**
 * Delegate used to broadcast attribute events, some of these parameters may be null on clients:
 * @param EffectInstigator	The original instigating actor for this event
 * @param EffectCauser		The physical actor that caused the change
 * @param EffectSpec		The full effect spec for this change
 * @param EffectMagnitude	The raw magnitude, this is before clamping
 * @param OldValue			The value of the attribute before it was changed
 * @param NewValue			The value after it was changed
*/
DECLARE_MULTICAST_DELEGATE_SixParams(FMetaAttributeEvent, AActor* /*EffectInstigator*/, AActor* /*EffectCauser*/, const FGameplayEffectSpec* GetGameplayAttributeValueChangeDelegate /*EffectSpec*/, float /*EffectMagnitude*/, float /*OldValue*/, float /*NewValue*/);

UCLASS()
class CLIENT_API UMetaAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
public:

	UMetaAttributeSet();
	virtual UWorld* GetWorld() const override;
	UMetaAbilitySystemComponent* GetMetaAbilitySystemComponent() const;
};
