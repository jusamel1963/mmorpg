// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Attributes/MetaAttributeSet.h"
#include "AbilitySystemComponent.h"
#include "MetaHealthSet.generated.h"


UCLASS()
class CLIENT_API UMetaHealthSet : public UMetaAttributeSet
{
	GENERATED_BODY()
public:
	UMetaHealthSet();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UFUNCTION() 
	void OnRep_Health(const FGameplayAttributeData& OldValue);
	UFUNCTION()	
	void OnRep_MaxHealth(const FGameplayAttributeData& OldValue);

	virtual bool PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	virtual void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;

	void ClampAttribute(const FGameplayAttribute& Attribute, float& NewValue) const;

public:
	ATTRIBUTE_ACCESSORS(UMetaHealthSet, Health);
	ATTRIBUTE_ACCESSORS(UMetaHealthSet, MaxHealth);
	ATTRIBUTE_ACCESSORS(UMetaHealthSet, Healing);
	ATTRIBUTE_ACCESSORS(UMetaHealthSet, Damage);

	// Delegate when health changes due to damage/healing, some information may be missing on the client
	mutable FMetaAttributeEvent OnHealthChanged;
	// Delegate when max health changes
	mutable FMetaAttributeEvent OnMaxHealthChanged;
	// Delegate to broadcast when the health attribute reaches zero
	mutable FMetaAttributeEvent OnOutOfHealth;

private:
	// The current health attribute.  The health will be capped by the max health attribute.  Health is hidden from modifiers so only executions can modify it.
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_Health, Category = "Meta|Health", Meta = (HideFromModifiers, AllowPrivateAccess = true))
	FGameplayAttributeData Health;

	// The current max health attribute.  Max health is an attribute since gameplay effects can modify it.
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_MaxHealth, Category = "Meta|Health", Meta = (AllowPrivateAccess = true))
	FGameplayAttributeData MaxHealth;

	// Incoming healing. This is mapped directly to +Health
	UPROPERTY(BlueprintReadOnly, Category = "Meta|Health", Meta = (AllowPrivateAccess = true))
	FGameplayAttributeData Healing;

	// Incoming damage. This is mapped directly to -Health
	UPROPERTY(BlueprintReadOnly, Category = "Meta|Health", Meta = (HideFromModifiers, AllowPrivateAccess = true))
	FGameplayAttributeData Damage;

	// Used to track when the health reaches 0.
	bool bOutOfHealth;

	float MaxHealthBeforeAttributeChange;
	float HealthBeforeAttributeChange;
};
