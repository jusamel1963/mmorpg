// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/CommonLocalPlayer.h"
#include "MetaLocalPlayer.generated.h"

/**
 * 
 */
UCLASS()
class CLIENT_API UMetaLocalPlayer : public UCommonLocalPlayer
{
	GENERATED_BODY()
	
};
