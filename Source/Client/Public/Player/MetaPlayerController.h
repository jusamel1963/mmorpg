// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/CommonPlayerController.h"
#include "MetaPlayerController.generated.h"

class UMetaInventoryManagerComponent;
class UMetaQuickBarComponent;
class UMetaWeaponStateComponent;

UCLASS()
class CLIENT_API AMetaPlayerController : public ACommonPlayerController
{
	GENERATED_BODY()
public:
	AMetaPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//~APlayerController interface
	//virtual void ReceivedPlayer() override;
	//virtual void PlayerTick(float DeltaTime) override;
	//virtual void SetPlayer(UPlayer* InPlayer) override;
	//virtual void AddCheats(bool bForce) override;
	//virtual void UpdateForceFeedback(IInputInterface* InputInterface, const int32 ControllerId) override;
	//virtual void UpdateHiddenComponents(const FVector& ViewLocation, TSet<FPrimitiveComponentId>& OutHiddenComponents) override;
	virtual void PreProcessInput(const float DeltaTime, const bool bGamePaused) override;
	virtual void PostProcessInput(const float DeltaTime, const bool bGamePaused) override;
	//~End of APlayerController interface

	UFUNCTION(BlueprintCallable, Category = "Meta|PlayerController")
	AMetaPlayerState* GetMetaPlayerState() const;

	UFUNCTION(Exec)
	void GiveWeapon();

public:
	UFUNCTION(BlueprintCallable, Category = "Meta|PlayerController")
	UMetaAbilitySystemComponent* GeMetaAbilitySystemComponent() const;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Controller", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaInventoryManagerComponent> InventoryManagerComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Controller", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaQuickBarComponent> QuickBarComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Meta|Controller", Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMetaWeaponStateComponent> MetaWeaponStateComponent;
};
