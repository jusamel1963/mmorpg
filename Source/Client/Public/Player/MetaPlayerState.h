// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ModularPlayerState.h"
#include "AbilitySystemInterface.h"
#include "MetaPlayerState.generated.h"

class UMetaAbilitySystemComponent;
class UMetaHealthSet;
class UMetaPawnData;
class UMetaExperienceDefinition;
class UMetaCombatSet;

UENUM()
enum class EMetaPlayerConnectionType : uint8
{
	// An active player
	Player = 0,

	// Spectator connected to a running game
	LiveSpectator,

	// Spectating a demo recording offline
	ReplaySpectator,

	// A deactivated player (disconnected)
	InactivePlayer
};

UCLASS(Config = Game)
class CLIENT_API AMetaPlayerState : public AModularPlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AMetaPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	UMetaAbilitySystemComponent* GetMetaAbilitySystemComponent() const { return AbilitySystemComponent; }
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	EMetaPlayerConnectionType GetPlayerConnectionType() const { return MyPlayerConnectionType; }
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;
	virtual void ClientInitialize(AController* C) override;

	template <class T>
	const T* GetPawnData() const { return Cast<T>(PawnData); }
	void SetPawnData(const UMetaPawnData* InPawnData);

protected:
	UFUNCTION()
	void OnRep_PawnData();

private:
	void OnExperienceLoaded(const UMetaExperienceDefinition* CurrentExperience);

public:
	static const FName NAME_MetaAbilityReady;

protected:
	UPROPERTY(ReplicatedUsing = OnRep_PawnData)
	TObjectPtr<const UMetaPawnData> PawnData;

private:
	UPROPERTY(Replicated)
	EMetaPlayerConnectionType MyPlayerConnectionType;

	TObjectPtr<UMetaAbilitySystemComponent> AbilitySystemComponent;
	UPROPERTY()
	TObjectPtr<const UMetaHealthSet> HealthSet;
	UPROPERTY()
	TObjectPtr<const UMetaCombatSet> CombatSet;
};
