// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Client : ModuleRules
{
	public Client(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateDependencyModuleNames.AddRange(new string[] { "EnhancedInput", "NetCore" });

        PublicDependencyModuleNames.AddRange(
			new string[] { 
				"Core", 
				"CoreUObject", 
				"Engine", 
				"InputCore", 
				"EnhancedInput",
				"GameplayTags",
                "GameplayTasks",
                "GameplayAbilities",
                "ModularGameplay", 
				"GameFeatures",
                "GameplayMessageRuntime",
                "AIModule"
            }
		);
        SetupIrisSupport(Target);
    }
}
